package kurtlike.itmo;

import kurtlike.itmo.config.Board;

public class ChatSession {
    private long chatID;
    private WorkState currentState;
    private Board currentBoard;
    private String fileName;

    public void setChatID(long chatID) {
        this.chatID = chatID;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Board getCurrentBoard() {
        return currentBoard;
    }

    public void setCurrentBoard(Board currentBoard) {
        this.currentBoard = currentBoard;
    }
    public ChatSession(long chatID){
        this.chatID = chatID;
        currentState = WorkState.STARTED;
    }

    public void setCurrentState(WorkState currentState) {
        this.currentState = currentState;
    }

    public long getChatID() {
        return chatID;
    }

    public WorkState getCurrentState() {
        return currentState;
    }
    public void clear(){
        currentState = WorkState.STARTED;
        currentBoard = null;
        fileName = null;
    }
}
