package kurtlike.itmo;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import kurtlike.itmo.config.Config;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.io.*;


/**
 * Hello world!
 *
 */
public class App
{
    public static void main(String[] args) throws IOException {
        String confPath = "config.json";
        if(args.length > 0)confPath = args[0];
        Config config = readConfig(confPath);
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            FPGAPollingBot bot =new FPGAPollingBot(config);
            botsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
    private static Config readConfig(String filepath) throws IOException {

        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(filepath));
        return gson.fromJson(reader, Config.class);
    }
}
