package kurtlike.itmo;

import kurtlike.itmo.config.Board;
import kurtlike.itmo.config.Config;
import org.apache.commons.io.FileUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.File;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static kurtlike.itmo.WorkState.*;

public class FPGAPollingBot extends TelegramLongPollingBot {
  private final Config config;
  private ArrayList<ChatSession> sessions = new ArrayList<>();
  private ChatSession getChatSession(long id){
    for(ChatSession session:sessions){
      if(session.getChatID() == id){
        return session;
      }
    }
    ChatSession newSession = new ChatSession(id);
    sessions.add(newSession);
    return newSession;
  }
  public FPGAPollingBot(Config config){
    super(config.getToken());
    this.config = config;
  }
  @Override
  public String getBotUsername() {
    return "fpga_boot_bot";
  }
  @Override
  public void onUpdateReceived(Update update) {
    // We check if the update has a message and the message has text
    if (update.hasMessage()) {
      Message rcvMessage = update.getMessage();
      long chatId = rcvMessage.getChatId();
      SendMessage sendMessage = new SendMessage();
      sendMessage.setChatId(chatId);
      ChatSession session = getChatSession(chatId);
      if(isStarted(rcvMessage)){
        sendMessage.setText(createInfoMessage());
        session.clear();
        try {
          execute(sendMessage);
        } catch (TelegramApiException e) {
          throw new RuntimeException(e);
        }
      }
      switch (session.getCurrentState()){
        case STARTED:{
          createBoardChoose(sendMessage);
          session.setCurrentState(WAITFORBOARD);
          break;
        }
        case WAITFORBOARD:{
          String msgText = rcvMessage.getText();
          String sendText;
          Board board = findBoardByName(msgText);
          if(board != null){
            session.setCurrentBoard(board);
            sendText = "прикрепите файл прошивки";
            session.setCurrentState(WAITFORFILE);
            sendMessage.setReplyMarkup(new ReplyKeyboardRemove(true));
          } else {
            sendText = "Ошибка ввода, выберете устройство из представленных";
            createBoardChoose(sendMessage);
            session.setCurrentState(WAITFORBOARD);
          }
          sendMessage.setText(sendText);
          break;
        }
        case WAITFORFILE:{
          String sendText;
          if(!rcvMessage.hasDocument()){
            sendText = "Ошибка ввода, прикрепите файл прошивки";
            sendMessage.setText(sendText);
            break;
          }
          Document document = update.getMessage().getDocument();
          try {
            String filePath = session.getChatID()+"/"+document.getFileName();
            downloadMyFile(document, config.getServerDataPath()+filePath);
            session.setFileName(document.getFileName());
            session.setCurrentState(READYTOFLASH);
            sendText = "Файл Успешно загружен\n" +
                    "Выбраное устройство: "+session.getCurrentBoard().getName() +"\n" +
                    "Выбраная прошивка: "+ document.getFileName() + "\n" +
                    "Прошить (Да/нет)";

          } catch (IOException | TelegramApiException e) {
            sendText = "Ошибка ввода, прикрепите файл прошивки";

          }
          sendMessage.setText(sendText);
          break;
        }
        case READYTOFLASH:{
          String sendText;
          if(rcvMessage.getText().equals("Да")){
            String cmd = createShelScript(session);
            try {
              Runtime.getRuntime().exec(cmd);
              sendText = "Файл отправлен на прошивку";
              //String executeScript ="";
              //Runtime.getRuntime().exec(executeScript);
            } catch (IOException | IllegalArgumentException e) {
              sendText = "Ошибка отправки";
            }
            sendMessage.setText(sendText);
          }else{
            createBoardChoose(sendMessage);
          }
          session.setCurrentState(FINISHED);
          break;
        }
        default:{
          sendMessage.setText("Неожиданная ошибка");
          break;
        }
      }
      try {
        execute(sendMessage);
      } catch (TelegramApiException e) {
        throw new RuntimeException(e);
      }
      if(session.getCurrentState() == FINISHED) {
        session.setCurrentState(STARTED);
        session.clear();
      }
    }
  }
  public File getFilePath(Document document) throws TelegramApiException {
    GetFile getFile = new GetFile();
    getFile.setFileId(document.getFileId());
    return execute(getFile);
  }
  public void downloadMyFile(Document document,  String localFilePath) throws IOException, TelegramApiException {
    File file = getFilePath(document);
    java.io.File localFile = new java.io.File(localFilePath);
    InputStream is = new URL(file.getFileUrl(config.getToken())).openStream();
    FileUtils.copyInputStreamToFile(is, localFile);
  }
  public void createBoardChoose(SendMessage message) {
    String sendMsgText = "Выберете плату для прошивки";
    message.setText(sendMsgText);
    ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
    List<KeyboardRow> keyboard = new ArrayList<>();
    ArrayList<Board> boards = config.getBoards();
    for(Board board:boards){
      KeyboardRow row = new KeyboardRow();
      row.add(board.getName());
      keyboard.add(row);
    }
    keyboardMarkup.setKeyboard(keyboard);
    message.setReplyMarkup(keyboardMarkup);
  }
  private boolean isStarted(Message message){
    if(!message.hasText())return false;
    String msg = message.getText();
    switch (msg){
      case "/start":
      case "/restart":
        return true;
    }
    return false;
  }
  private String createInfoMessage(){
    return "Привет, это бот для прошивки ПЛИС";
  }
  private Board findBoardByName(String name){
    for(Board board:config.getBoards()){
      if(board.getName().equals(name)) return board;
    }
    return null;
  }
  private String createShelScript(ChatSession session){
    Board board = session.getCurrentBoard();
    return  "scp "+ config.getServerDataPath() + session.getChatID()+"/"+session.getFileName() +" "+
            board.getUserName()+"@"+board.getIp()+":"+board.getBoardPath()+ session.getFileName();
  }
}
