package kurtlike.itmo;

public enum WorkState {
    STARTED,
    WAITFORBOARD,
    WAITFORFILE,
    READYTOFLASH,
    FINISHED

}
