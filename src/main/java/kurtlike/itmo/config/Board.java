package kurtlike.itmo.config;

public class Board {
    private String name;
    private String ip;
    private String userName;
    private String boardPath;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBoardPath() {
        return boardPath;
    }

    public void setBoardPath(String boardPath) {
        this.boardPath = boardPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
