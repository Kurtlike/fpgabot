package kurtlike.itmo.config;

import java.util.ArrayList;

public class Config {
   private String token;
   private String botName;
   private ArrayList<Board> boards;
   private String serverDataPath;

    public String getServerDataPath() {
        return serverDataPath;
    }

    public void setServerDataPath(String serverDataPath) {
        this.serverDataPath = serverDataPath;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBotName() {
        return botName;
    }

    public void setBotName(String botName) {
        this.botName = botName;
    }

    public ArrayList<Board>getBoards() {
        return boards;
    }

    public void setBoards(ArrayList<Board> boards) {
        this.boards = boards;
    }

}
